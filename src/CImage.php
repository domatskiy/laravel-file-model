<?php

namespace Domatskiy\FileModel;

use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

abstract class CImage extends CFile
{
    const RESIZE_CROP = 1;
    const RESIZE_PROPORTIONAL = 2;

    public $resize_on_save = false;
    protected $save_max_height = 1200;
    protected $save_max_width = 1200;

    protected $disk_resize = 'public';

    protected $path_resize;

    protected $image_quality = 75;

    protected $watermark = null;
    protected $watermark_opacity = null;
    protected $watermark_text = null;

    public function __construct(array $attributes = [])
    {
        // set

        parent::__construct($attributes);
    }

    public function setQuality(int $quality):CImage
    {
        $this->image_quality = $quality;

        return $this;
    }

    public function setWatermark(string $path, int $opacity = 70):CImage
    {
        // reset
        $this->watermark_text = null;

        // set
        $this->watermark = $path;
        $this->watermark_opacity = $opacity;

        if ($this->watermark_opacity < 0 || $this->watermark_opacity > 100) {
            throw new \Exception('not correct watermark opacity');
        }

        return $this;
    }

    public function setWatermarkText(string $text, int $opacity = 70):CImage
    {
        // reset
        $this->watermark_text = null;

        // set
        $this->watermark = $text;
        $this->watermark_opacity = $opacity;

        if ($this->watermark_opacity < 0 || $this->watermark_opacity > 100) {
            throw new \Exception('not correct watermark opacity');
        }

        return $this;
    }

    /**
     * @desc declare event handlers
     */
    protected static function boot()
    {
        parent::boot();

        static::saved(function (CImage $image) {
            # если файл обновлен
            if (static::$fileUpdated === true) {
                $image->deleteResize();
            }
        });

        static::updated(function (CImage $image) {
            # если файл обновлен
            if (static::$fileUpdated == true) {
                $image->deleteResize();
            }
        });
    }

    /**
     * @desc хранилице ресайзов картинок
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    public function getResizeStorage()
    {
        return \Storage::disk($this->disk_resize);
    }


    /**
     * @desc получение папки с ресайзом картинки
     * @return string
     * @throws \Exception
     */
    public function getResizeDir()
    {
        if ($this->group_field == 'id') {
            if ((int)$this->id < 1) {
                throw new \Exception('empty id');
            }

            return $this->path_resize.DIRECTORY_SEPARATOR.$this->__getDiapasonFromID($this->id);
        } else {
            if ((int)$this->{$this->group_field} < 1) {
                throw new \Exception('empty '.$this->group_field);
            }

            return $this->path_resize.DIRECTORY_SEPARATOR.$this->__getDiapasonFromID($this->{$this->group_field}).DIRECTORY_SEPARATOR.$this->{$this->group_field};
        }
    }

    /**
     * @desc сохранение файла, полученного в __getFleInfo
     * @param CFile $file
     * @return string|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected static function __downloadFile(CImage $image)
    {
        $file_path = parent::__downloadFile($image);

        if ($file_path) {
            
			// exit if
            if (!$image->getStorage()->exitrs($file_path) || !$img->resize_on_save) {
                return $file_path;
            }

            $mimetype = $image->getStorage()->getMimetype($file_path);
            $file_path_abs = $image->getStorage()->path($file_path);

			
            if (strpos($mimetype, 'image/') === 0) {
                $img = InterventionImage::make($file_path_abs);
            }

            if ($img) {
                if ($img->height() > $image->save_max_height || $img->width() > $save_max_width) {
                    if ($img->height() > $img->width()) {
                        $img->resize(null, $image->save_max_height, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    } else {
                        $img->resize($image->save_max_width, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                }

                $img->save(null, 95);
            }
        }
		
		return $file_path;
    }

    /**
     * @desc Удаление всех ресайзов картинки
     */
    public function deleteResize()
    {
        $disk = \Storage::disk($this->disk_resize);

        try {
            $path = $this->getResizeDir();

            if ($path) {
                # список файлов в директории
                $files = $disk->files($path, false);

                $delete_count = 0;

                foreach ($files as $file) {
                    # удаляем файлы ресайзов для данной картинки <image id>_<size>
                    if (strpos($file, $path.'/'.$this->id.'_') === 0) {
                        $disk->delete($file);
                        $delete_count++;
                    }
                }

                # если все фалы удалены, удаляем раздел
                if (count($files) == $delete_count) {
                    $disk->deleteDir($path);
                }
            }
        } catch (\Exception $e) {
            //
        }
    }

    /**
     * @desc создание ресайза
     * @param int $w
     * @param int $h
     * @param bool $set_watermark
     * @param int $resize_type
     * @return bool|null|string
     * @throws \Exception
     */
    public function resizeImage($w = 400, $h = 400, int $resize_type = 2)
    {
        if (!$this->path_resize || !is_string($this->path_resize)) {
            throw new \Exception('setting: not correct path for file');
        }

        if (!$this->{$this->field_code_file}) {
            throw new \Exception('field '.$this->field_code_file.' not exits or empty');
        }

        $disk = \Storage::disk($this->disk);
        $disk_rz = \Storage::disk($this->disk_resize);

        if (!$disk->exists($this->getPath())) {
            throw new \Exception('image path not exits: '.$this->getPath());
        }

        $info = pathinfo($this->{$this->field_code_file});

        # TODO watermark text

        $resize_name = $this->id.'_'.$w.'x'.$h.($this->watermark ? '_wm' : '').($this->image_quality === 70 ? '' : '_q'.$this->image_quality).'.'.$info['extension'];
        $resize_dir = $this->getResizeDir();
        $resize_path = $resize_dir.DIRECTORY_SEPARATOR.$resize_name;

		// description http://image.intervention.io/
		
        try {
			// check file
            if (!$disk_rz->exists($resize_path)) {
                
				// create dir
                if (!$disk_rz->exists($resize_dir)) {
                    $disk_rz->makeDirectory($resize_dir);
                }

                $image_path = realpath($disk->path($this->getPath()));
                $img = \Intervention\Image\Facades\Image::make($disk->get($this->getPath()));

				// требуется уменьшение?
                if ($img->height() > $h || $img->width() > $w) {
                    if ($resize_type === self::RESIZE_PROPORTIONAL) {
						# пропорциоальное уменьшение
                        if ($img->height() > $img->width()) {
                            $img->resize(null, $h, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                        } else {
                            $img->resize($w, null, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                        }
                    } else {
						# уменьшение с обрезкой
                        $сh = $img->height() / $h;
                        $cw = $img->width() / $w;

                        if ($сh > $cw) {
                            # уменьшение по ширине больше
                            $img->resize($w, null, function ($constraint) {
                                $constraint->aspectRatio();
                                #$constraint->upsize();
                            });
                        } else {
                            # уменьшение по высоте
                            $img->resize(null, $h, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                        }

						// обрезка
                        $img->crop($w, $h);
                    }
                }
				
				#---------------------------------------------------------
				# установка вотермарка
				#---------------------------------------------------------
                if ($img && $this->watermark) {
                    $watermark = InterventionImage::make($_SERVER['DOCUMENT_ROOT'].$this->watermark);

                    if ($watermark->height() > $h || $watermark->width() > $w) {
                        if ($watermark->height() > $h) {
                            $watermark->resize(null, $h - 100, function ($constraint) {
                                $constraint->aspectRatio();
                                #$constraint->upsize();
                            });
                        } else {
                            $watermark->resize($w - 100, null, function ($constraint) {
                                $constraint->aspectRatio();
                                #$constraint->upsize();
                            });
                        }
                    }

                    #$watermark->rotate(0);
                    $watermark->opacity(50);

                    if ($watermark) {
                        $img->insert($watermark, 'center-center'); // bottom-right
                        #\Log::debug('watermark for '.$resize_path);
                    }
                }

                # save image
                $img->save($disk_rz->path($resize_path), $this->image_quality);
            }

            return $resize_path;
        } catch (\Exception $e) {
            \Log::error('ups resize for '.$resize_path.': '.$e->getMessage().' file '.$e->getFile().' line '.$e->getLine()."\n".$e->getTraceAsString());
            return null;
        }
    }

    /**
     * @ получение url ресайза картики по пути к ресайзу
     * @param $path
     * @return string
     */
    public function getResizeUrl($path)
    {
        return $this
            ->getResizeStorage()
            ->url($path);
    }

    /**
     * @param string $path
     * @return \stdClass
     */
    public function getResizeSize(string $path): \stdClass
    {
        $p = $this->getResizeStorage()->path($path);
        $img = Image::make($p);

        $size = new \stdClass();
        $size->w = $img->getWidth();
        $size->h = $img->getHeight();

        return $size;
    }

    /**
     * @param $path
     * @return array
     */
    public function getResize($path)
    {
        if ($this->getResizeStorage()->exists($path)) {
            $p = $this->getResizeStorage()->path($path);
            $content = $this->getResizeStorage()->get($path);
            $mimetype = $this->getResizeStorage()->getMimetype($path);
            $metadata = $this->getResizeStorage()->getMetadata($path);
            $size = $this->getResizeStorage()->getSize($path);

            return [
                'abs_path' => $p,
                'content' => $content,
                'size' => $size,
                'metadata' => $metadata,
                'mimetype' => $mimetype,
            ];
        }

        return null;
    }
}
