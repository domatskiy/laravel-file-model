<?php

namespace Domatskiy\FileModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;

abstract class CFile extends Model
{
    /**
     * @var
     */
    protected static $url;

    /**
     * @var
     */
    protected static $fileUpdated;

    /**
     * @var string
     */
    protected $disk = 'local';

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $group_field = 'id';

    /**
     * @var string
     */
    protected $field_code_file = 'file';

    /**
     * @desc группировка файлов по
     * @var int
     */
    protected $diapason = 1000;

    public function __construct(array $attributes = [])
    {
        if (!$this->path || !is_string($this->path)) {
            throw new \Exception('setting: not correct path for file');
        } else {
            $this->path = trim($this->path, DIRECTORY_SEPARATOR);
        }

        if ((int)$this->diapason < 1) {
            throw new \Exception('setting: not correct diapason for file');
        }

        if (!$this->group_field || !is_string($this->group_field)) {
            throw new \Exception('setting: not correct group_field for file');
        }

        if (!$this->field_code_file || !is_string($this->field_code_file)) {
            throw new \Exception('setting: not correct field code of file');
        }

        if (!$this->table || !is_string($this->table)) {
            throw new \Exception('setting: not correct table of file');
        }

        parent::__construct($attributes);
    }

    /**
     * @desc declare event handlers
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function (CFile $file) {
            \DB::beginTransaction();
            self::__getFleInfo($file);
        });

        static::saved(function (CFile $file) {
            try {
                self::__downloadFile($file);
                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollBack();
                throw $e;
            }
        });

        static::deleting(function (CFile $file) {
            try {
                $disk = $file->getStorage();

                # удаляем
                $res = null;
                if ($disk->has($file->getPath())) {
                    $res = $disk->delete($file->getPath());
                }

                # после удаления проверить директорию
                if ($disk->has($file->getDir())) {
                    $files = $disk->files($file->getDir());

                    # если нет файлов - удалить
                    if (count($files) == 0) {
                        $disk->deleteDirectory($file->getDir());
                    }
                }

                return $res;
            } catch (\Exception $e) {
                //
            }
        });
    }

    /**
     * @desc получение значения поля file для дальнейшего охранения
     * @param CFile $file
     * @throws \Exception
     */
    protected static function __getFleInfo(CFile &$file)
    {
        if (!$file || !isset($file->{$file->field_code_file}) || !$file->{$file->field_code_file}) {
            return;
        }

        $tmp = $file->{$file->field_code_file};
        $isRemote = is_string($tmp) && strpos($tmp, 'http') === 0;

        if ($isRemote || $tmp instanceof UploadedFile) {
            $file->{$file->field_code_file} = '';
            static::$url = $tmp;
            static::$fileUpdated = true;
        }
    }

    /**
     * @desc сохранение файла, полученного в __getFleInfo
     * @param CFile $file
     * @return string|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected static function __downloadFile(CFile $file)
    {
        # Log::debug('url '.static::$url);

        if (!static::$url) {
            return null;
        }

        $upload_file = static::$url;

        // для избежания зацикливания
        static::$url = null;

        if ($upload_file instanceof UploadedFile) {
            # Log::debug('url is UploadedFile');

            if (!$file->id) {
                throw new \Exception('Set id for save file');
            }

            // Illuminate\Http\UploadedFile
            /**
             * @var $file UploadedFile
             */

            $file_name = $file->id.($upload_file->extension() ? '.'.$upload_file->extension() : '');
            $file_original = $upload_file->getClientOriginalName();

            #Log::debug('$file_name='.$file_name);
            #Log::debug('$file_original='.$file_original);

            #\Log::debug('__downloadFile ... storeAs '.$file->getDir().DIRECTORY_SEPARATOR.$file_name);

            $upload_file->storeAs($file->getDir(), $file_name, $file->getDisk());

            // $file->file_original = $file_original;

            #\Log::debug('__downloadFile ... update');
            $file->update([
                $file->field_code_file => $file_name
            ]);

            return $file->getDir().DIRECTORY_SEPARATOR.$file_name;
        } elseif (is_string($upload_file)) {
            $pathinfo = pathinfo($upload_file);

            if (!filter_var($pathinfo['dirname'], FILTER_VALIDATE_URL)) {
                throw new \Exception('Not valid url '.$upload_file);
            }

            if (!$file->id) {
                throw new \Exception('Set id for save file');
            }

            $basename = explode('&', $pathinfo['basename']);
            $basename = explode('?', $basename[0]);

            $extension = explode('&', $pathinfo['extension']);
            $extension = explode('?', $extension[0]);

            #\Log::debug('__downloadFile ... update');
            $file->update([
                'file' => $file->id.( $extension[0] ? '.'.$extension[0] : ''),
                'file_original' => isset($basename[0]) ? $basename[0] : '',
            ]);

            #\Log::debug('CFile download '.$upload_file);

            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $upload_file, [
                'timeout' => 20
            ]);

            if ($res->getStatusCode() !== 200) {
                throw new \Exception('Error download file '.$upload_file.', status='.$res->getStatusCode());
            }

            $file_content = $res->getBody();

            /*if (!$file_content) {
                throw new \Exception('Error download, empty file '.$upload_file);
            }*/

            $file_path = $file->getPath();
            
            $disk = $file->getStorage();
            $disk->put($file_path, $file_content);

            return $file_path;
        }

        return null;
    }

    /**
     * @desc хранилице файлов
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    public function getStorage()
    {
        return \Storage::disk($this->disk);
    }

    /**
     * @ получить название диска
     * @return string
     */
    public function getDisk()
    {
        return $this->disk;
    }

    /**
     * @param $id
     * @return string
     * @throws \Exception
     */
    protected function __getDiapasonFromID($id)
    {
        $id = intval($id);

        if ($id < 1) {
            throw new \Exception('not correct id: '. $id);
        }

        $p = floor(($id - 1) / $this->diapason);

        $l2 = ($p+1) * $this->diapason;
        $l1 = $l2 - $this->diapason + 1;

        return $l1.'-'.$l2;
    }

    /**
     * @desc получение папки для картинки
     * @return string
     * @throws \Exception
     */
    public function getDir()
    {
        if ($this->group_field == 'id') {
            if ((int)$this->id < 1) {
                throw new \Exception('empty id');
            }

            return $this->path.DIRECTORY_SEPARATOR.$this->__getDiapasonFromID($this->id);
        } else {
            if ((int)$this->{$this->group_field} < 1) {
                throw new \Exception('empty '.$this->group_field);
            }

            return $this->path.DIRECTORY_SEPARATOR.$this->__getDiapasonFromID($this->{$this->group_field}).DIRECTORY_SEPARATOR.$this->{$this->group_field};
        }
    }

    /**
     * @desc полный путь до файла
     * @return string
     * @throws \Exception
     */
    public function getPath()
    {
        if (!isset($this->{$this->field_code_file}) || !$this->{$this->field_code_file}) {
            throw new \Exception('empty file field for path');
        }

        return $this->getDir().DIRECTORY_SEPARATOR.$this->file;
    }
}
