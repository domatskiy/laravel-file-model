<?php
/**
 * Created by PhpStorm.
 * User: domatskiy
 * Date: 02.03.2019
 * Time: 9:31
 */

namespace Domatskiy\FileModel\Test;

class File extends \Domatskiy\FileModel\CFile
{
    protected $table = 'file';

    protected $path = 'test';

    /**
     * Атрибуты, исключенные из JSON-представления модели.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');

    protected $fillable = array(
        'file',
    );

}
