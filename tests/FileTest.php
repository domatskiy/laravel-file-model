<?php

namespace Domatskiy\FileModel\Test;

use Domatskiy\FileModel\CFile;
use Domatskiy\FileModel\CImage;
use Illuminate\Http\UploadedFile;
use Orchestra\Testbench\TestCase as TestCase;

require_once 'File.php';
require_once 'Image.php';

/**
 * Class FileTest
 * @package Domatskiy\FileModel\Test
 */
class FileTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            \Intervention\Image\ImageServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Image' => \Intervention\Image\Facades\Image::class
        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('filesystems.disks', [
            'local' => [
                'driver' => 'local',
                'root' => storage_path('app'),
            ],
            'public' => [
                'driver' => 'local',
                'root' => storage_path('app/public'),
                'url' => config('app.url').'/storage',
                'visibility' => 'public',
            ],
        ]);

        $app['config']->set('catalog.driver', 'local');

        $app['config']->set('database.default', 'testing');
        $app['config']->set('database.connections.testing', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    /**
     * Resolve application Console Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function resolveApplicationConsoleKernel($app)
    {
        #$app->singleton('Illuminate\Contracts\Console\Kernel', 'Illuminate\Foundation\Console\Kernel');
        $app->singleton('Illuminate\Contracts\Console\Kernel', '\Orchestra\Testbench\Console\Kernel');
    }

    public function setUp()
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadLaravelMigrations(['--database' => 'testing']);
    }

    /** @test */
    public function it_runs_the_migrations()
    {
        /**
         * @var $file CFile
         */

        $file = File::create([
            'file' => new UploadedFile(__DIR__.DIRECTORY_SEPARATOR.'file.txt', 'file.txt'),
        ]);

        $this->assertTrue(isset($file->file) && strlen($file->file), 'empty value');

        $file_test = File::find($file->id);
        var_dump($file_test->toArray());
        $this->assertTrue($file_test->getPath() === $file->getPath(), 'not correct file path');

        echo PHP_EOL;
        echo 'file path = '.$file->getStorage()->path($file->getPath()).PHP_EOL;

        /**
         * @var $img CImage
         */
        $img = Image::create([
            'file' => new UploadedFile(__DIR__.DIRECTORY_SEPARATOR.'abs.jpg', 'abs.jpg'),
        ]);

        $this->assertTrue(isset($img->file) && strlen($img->file), 'image file field empty value');

        echo 'save image to '.$img->file.PHP_EOL;

        /**
         * TEST IMAGE
         */

        $rsz = $img
            ->setQuality(80)
            # TODO ->setWatermark(__DIR__.DIRECTORY_SEPARATOR.'watermark.png')
            ->resizeImage(200, 200, 2);

        echo 'image resize path = '.$rsz.PHP_EOL;
        $this->assertTrue(strlen($rsz) > 0, 'image resize empty value '.$rsz);

        if ($rsz) {
            $url = $img->getResizeUrl($rsz);
            echo 'getResizeUrl: '.$url.PHP_EOL;
            $this->assertTrue(strlen($url) > 0, 'image resize empty value '.$url);

            $size = $img->getResizeSize($rsz);
            $this->assertObjectHasAttribute('w', $size,  'image size attr not exit "w"');
            $this->assertObjectHasAttribute('h', $size,  'image size attr not exit "h"');

        }
    }
}
