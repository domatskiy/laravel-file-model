<?php
/**
 * Created by PhpStorm.
 * User: domatskiy
 * Date: 02.03.2019
 * Time: 9:31
 */

namespace Domatskiy\FileModel\Test;

class Image extends \Domatskiy\FileModel\CImage
{
    protected $table = 'image';

    protected $path = 'test_img';

    public $resize_on_save = false;

    protected $path_resize = 'test_img_rsz';

    /**
     * Атрибуты, исключенные из JSON-представления модели.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');

    protected $fillable = array(
        'file',
    );
}
