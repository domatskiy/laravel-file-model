# Laravel File Model

###Install

```bash
composer require domatskiy/laravel-file-model
```

*File*
```php
class File extends \Domatskiy\FileModel\CFile
{
    protected $table = 'file';
}
```

```php
class File extends \Domatskiy\FileModel\CFile
{
    protected $table = 'file';

    protected $path = 'test';

    protected $hidden = array('created_at', 'updated_at');

    protected $fillable = array(
        'file',
    );
}
```
save file
```php
$file = File::create([
    'file' => new UploadedFile(__DIR__.'/file.txt', 'file.txt'),
]);
```
  
####Images
```php
class Image extends \Domatskiy\FileModel\CImage
{
    protected $table = 'file2';
   
    protected $path = 'test2';

    protected static $resize_on_save = false;
    protected $save_max_height = 1200;
    protected $save_max_width = 1200;

    protected $disk_resize = 'public';

    protected $path_resize = 'resize';

    protected $image_quality = 65;
    
    protected $hidden = array('created_at', 'updated_at');

    protected $fillable = array(
        'file',
    );
}
```

save image and get resize
```php
    $img = Image::create([
        'file' => new UploadedFile(__DIR__.'/abs.jpg', 'abs.jpg'),
    ]);
    
    # get resize
    $rsz = $img
            ->setQuality(80)
            ->getResizeImage(200, 200, $img::RESIZE_PROPORTIONAL);
            
    if ($rsz) {
        $path = $img->getResizeUrl($rsz); 
    }
```
